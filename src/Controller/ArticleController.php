<?php

namespace App\Controller;

use App\Service\MarkdownHelper;
use App\Service\SlackClient;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;

class ArticleController extends AbstractController
{
    /**
     * Currently unused: just showing a controller with a constructor!
     */
    private $isDebug;

    public function __construct(bool $isDebug)
    {
        $this->isDebug = $isDebug;
    }

    /**
     * @Route("/", name="app_homepage")
     */
    public function homepage()
    {
        return $this->render('article/homepage.html.twig');
    }

    /**
     * @Route("/agence", name="app_agencepage")
     */
    public function agencepage()
    {
        return $this->render('article/agencepage.html.twig');
    }

    /**
     * @Route("/aerosol", name="app_aerosolpage")
     */
    public function aerosolpage()
    {
        return $this->render('article/aerosolpage.html.twig');
    }


    /**
     * @Route("/prestation", name="app_prestationpage")
     */
    public function prestationpage()
    {
        return $this->render('article/prestationpage2.html.twig');
    }


    /**
     * @Route("/realisation", name="app_realisationpage")
     */
    public function realisationpage()
    {
        return $this->render('article/realisationpage.html.twig');
    }

    /**
     * @Route("/contact", name="app_contactpage")
     */
    public function contactpage()
    {
        return $this->render('article/contactpage.html.twig');
    }

    /**
     * @Route("/event", name="app_eventpage")
     */
    public function eventpage()
    {
        return $this->render('article/eventpage.html.twig');
    }

    /**
     * @Route("/teambuilding", name="app_teambuildingpage")
     */
    public function teambuildingpage()
    {
        return $this->render('article/teambuilding.html.twig');
    }

    /**
     * @Route("/decoration", name="app_decorationpage")
     */
    public function decorationpage()
    {
        return $this->render('article/decoration.html.twig');
    }

    /**
     * @Route("/presse", name="app_pressepage")
     */
    public function pressepage()
    {
        return $this->render('article/presse.html.twig');
    }



}
